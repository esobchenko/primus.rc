set nocompatible

set nowrap " don't wrap lines
set tabstop=4 " a tab is four spaces

" allow backspacing over everything in insert mode
set backspace=indent,eol,start "

set hidden

" always set autoindenting on
set autoindent

" copy the previous indentation on autoindenting
set copyindent

" number of spaces to use for autoindenting
set shiftwidth=4

set nobackup
set noswapfile

set backspace=2
set formatoptions=tcrq2
set helpheight=16

set history=1000    " remember more commands and search history
set undolevels=1000 " use many muchos levels of undo

set title " change terminal title

set smarttab

set wildignore=*.swp,*.bak,*.pyc,*.class

set ignorecase
set infercase
set nojoinspaces
set linebreak
set more
set modelines=3
set ruler
set showbreak=+\
set showcmd
set showmatch

set splitbelow

set terse

set visualbell
set noerrorbells

set cmdheight=2
set whichwrap=b,s,h,l,<,>,[,]

set expandtab

set list
set listchars=tab:>-,trail:@,eol:¬
nmap <f1> :set nolist!<CR>

" Intuitive backspacing in insert mode
set backspace=indent,eol,start

"map <up> <nop>
"map <down> <nop>
"map <left> <nop>
"map <right> <nop>

" File-type highlighting and configuration.
" Run :filetype (without args) to see what you may have
" to turn on yourself, or just set them all to be sure.
syntax on
filetype on
filetype plugin on
filetype indent on

" Highlight search terms...
set hlsearch
set incsearch " ...dynamically as they are typed.
nmap <f2> :silent :nohlsearch<CR>
nmap <f1> :%!perltidy -q<CR>

set encoding=utf-8
colorscheme solarized

set guioptions-=T " no toolbar
set guioptions-=m " no menubar
" set guioptions-=r " no scrollbar
set guioptions+=c " console dialogs only

" no scrollbars
set guioptions+=LlRrb
set guioptions-=LlRrb

set guifont=Terminus

set backupdir=~/t,~/tmp,/var/tmp,/tmp
set directory=~/t,~/tmp,/var/tmp,/tmp
set shortmess=atI

" command Tidy :%!perltidy -q

" runtime macros/matchit.vim

" Run NERDTree automatically if no files specified for editing
" autocmd vimenter * if !argc() | NERDTree | endif

" Toggle NERDTree on <F3> and <F4>
" nmap <f3> :NERDTreeToggle<CR>
" nmap <f4> :NERDTreeToggle<CR>




