
HISTFILESIZE=10000


# /usr/local/bin is before /usr/bin to avoid unzip tool issue that affects perl compilation using perlbrew
export PATH=/sbin:/bin:/usr/sbin:/usr/games:/usr/local/sbin:/usr/local/bin:/usr/bin:$HOME/bin:$HOME/postgres/bin

export TERM=xterm-256color
export EDITOR=vim
export PAGER=less

export LANG="en_US.UTF-8"
export LC_COLLATE="ru_RU.UTF-8"
export LC_CTYPE="ru_RU.UTF-8"

export PERLDOC_PAGER="less -sR"

source /usr/local/share/git-core/contrib/completion/git-completion.bash
source /usr/local/share/git-core/contrib/completion/git-prompt.sh

# we should undef MANPATH after loading perlbrew's bashrc because
# it defines it wrong so most of manpages become invisible for man(8)
source ~/perl5/perlbrew/etc/bashrc
export MANPATH=

export PS1='\u@\h:\w$(__git_ps1 " (%s)") \$ '

set -o vi

# default data directory for postgres daemon
export PGDATA="/home/eugen/postgres/data"
