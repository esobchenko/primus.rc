DIRS			?= boot etc home usr

TIMESTAMP		!= date +%Y%m%d_%H%M%S

all: .PHONY backup bootstrap

backup: .PHONY
	find $(DIRS) -type f | sed 's/^/\//' | xargs -I XX sh -c 'test -f XX && echo XX' | xargs -I XX cp XX XX.${TIMESTAMP}

bootstrap: .PHONY backup
	find $(DIRS) -type f | xargs -I XX sh -c 'test -f /XX && echo XX' | xargs -I XX cp -v XX "/XX"




